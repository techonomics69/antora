'use strict'

/**
 * Redirect Producer component for Antora
 */
module.exports = require('./produce-redirects')
