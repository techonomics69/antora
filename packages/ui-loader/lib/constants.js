'use strict'

module.exports = Object.freeze({
  SUPPLEMENTAL_FILES_GLOB: '**/*',
  UI_CACHE_FOLDER: 'ui',
  UI_CONFIG_FILENAME: 'ui.yml',
})
