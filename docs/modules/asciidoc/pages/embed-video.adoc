= Embed a Video
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
// Settings
:idprefix:
:idseparator: -
:linkattrs:
// External URLs
:uri-adoc-manual: https://asciidoctor.org/docs/user-manual
:uri-video-formats: https://developer.mozilla.org/en-US/docs/Web/HTML/Supported_media_formats#Browser_compatibility
:uri-hosted-video: {uri-adoc-manual}/#video
:uri-shared-video: {uri-adoc-manual}/#youtube-and-vimeo-videos
:uri-video-attrs: {uri-adoc-manual}/#supported-attributes

On this page, you'll learn:

* [x] How to embed a video in a page.
* [x] How to embed a YouTube or Vimeo video in a page.

== Add a video to a page

You can embed self-hosted videos or videos shared on YouTube and Vimeo.

Video format support is dictated by the user's browser and/or system.
For a list of the web video formats each browser supports, see the {uri-video-formats}[Mozilla Developer Supported Media Formats documentation^].

TIP: Insert animated GIFs with the xref:insert-image.adoc[image macro].

.Embedded self-hosted video syntax
----
video::video-file.mp4[]
----

Let's break this down.
You start with the video macro prefix, `video::`.
Next is the target.
Put the path of the video relative to the xref:ROOT:modules.adoc#videos-dir[video catalog] in that slot (no need for an attribute prefix).
Finally, end with a pair of square brackets (`+[]+`).

== Add a YouTube or Vimeo video

To embed a video hosted on YouTube or Vimeo, put the video's ID in the macro target and the name of the hosting service (`youtube`, `vimeo`) between the brackets.

.Embedded Youtube video syntax
----
video::rPQoq7ThGAU[youtube]
----

[discrete]
==== Asciidoctor resources

* {uri-hosted-video}[The AsciiDoc video macro^]
* {uri-shared-video}[Embed Youtube and Vimeo videos^]
* {uri-video-attrs}[Video macro options and attributes^]
