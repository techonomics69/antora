= Use Author Mode
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
// Settings
:idprefix:
:idseparator: -
:experimental:

* [x] How to activate author mode.

Although the primary function of Antora is to generate a site for publishing, it also serves as a tool for the author.
By configuring Antora to use a local repository, you can preview local content, including content you haven't committed.

== Activate author mode

Author mode is activated when you configure the playbook to use a local clone for one or more of the content repositories.
Antora will use the repository as it is on your local machine instead of cloning it from its remote location.
This scenario assumes you've already cloned a repository and want to incorporate it into the site generation.

To begin, create a folder named [.path]_workspace_ in your project.

 $ mkdir workspace

Switch to the newly created directory and clone one of the repositories:

 $ cd workspace &&
   git clone https://github.com/my-antora-demo/server-docs

Next, open your playbook file and configure it to use this clone.
You can specify the repository either as a path relative to the project root or as an absolute path.

// also a good place to use the demo
// need clarification about where these local sources can be in relation to the playbook project

.site.yml (excerpt)
[source,yaml]
----
content:
  sources:
  - url: ./workspace/server-docs
----

Now when you run Antora, the contents of your local repository will be incorporated into the pipeline.

== Author mode order of operations

You may be wondering what branches the generator selects when the repository is local and whether it will pick up your uncommitted changes.
Here's how a local repository is handled:

* The repository contents on your local machine is used instead of its contents from its remote storage location.
//being cloned from its remote storage location  into build/sources.
* The local repository is not updated from its remote storage location; Antora assumes the author will manage the repository (explicitly fetch or pull the repository as needed).

Here's how the branches are selected in author mode:

* Both local branches and remote branches associated with the remote named origin are considered.
* If a local branch has the same name as a remote branch, the local branch is chosen.
* The contents of the working tree get used in place of the files from the current branch.
Aside from its name, the current branch is effectively ignored.

If you want to use multiple working trees, simply clone the repository multiple times and configure multiple entries in the playbook.
You can use the `branches` key to xref:configure-content-sources.adoc#branches[filter out the names of branches you don't want].
