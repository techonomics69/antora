= Features
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
// Settings
:idprefix:
:idseparator: -
:experimental:

// on website as of 1/15
A static site generator for making documentation sites from one or more versioned content repositories.

////
Alternatives

A static site generator for teams writing and managing documentation for multiple products, versions, and site environments.

A documentation pipeline that can manage multiple of source repositories, version branches, and publishing environments.

A modular, portable pipeline built for docs and engineering teams with multiple products, workflow policies, and publishing environments.

A modular documentation pipeline that adapts to team workflows and technology shifts.
Built on the best practices of docs as code and continuous integration and delivery.

Documentation pipeline
built for changing technical and team requirements
built on continuous delivery best practices
give writers the ability to create & manage their documentation content and sites, without needing a deep understanding of web tech; don't need to be sysadmins or developers doesn't require expertise in web technologies, build automation, or system administration

Short form
An Asciidoctor documentation toolchain and workflow that empowers technical writing teams to create, manage, collaborate on, remix, and publish documentation sites sourced from a variety of versioned content repositories without needing expertise in web technologies, build automation, or system administration.

From site data file
An Asciidoctor documentation toolchain that helps technical teams create, manage, collaborate on, remix, release, and publish documentation sites sourced from multiple versioned repositories.

From project site footer
A documentation pipeline that enables docs, product, and engineering teams to create, manage, remix, and publish documentation sites composed in AsciiDoc and sourced from multiple versioned repositories.
////

== Docs as Code

=== Store documentation in multiple repositories
// Store documentation in 1, 42 or more repositories.
// One, 42, or more repositories

// on website
No matter how many git repositories you use, Antora can retrieve and aggregate all the content from them to assemble a documentation site.

=== Manage documentation versions like a pro
// Version your docs like a pro
// Manage versions like a pro

// on website
Use branches to version documentation just like software.
Antora can find repository branches and assemble the files in each branch into a versioned documentation collection.
Antora also knows how to ignore branches you don’t want published.

=== Workspaces that fit team and content requirements
// Store documentation in its own repository or in the same repository as the code it describes.
// Storage locations that work for writers and subject matter experts

// on website
Since documentation files can be stored in separate repositories, the teams that maintain them can use the administration policies, version schema, and release schedule that works for them.

////
notes

Whether you store the documentation for a product in its own repository, in that same repository as the product's code, or the text pages in one repository and the example code in another repository, Antora can find and assemble the files into a documentation site.

The source repository combinations are almost endless with Antora.
Store the documentation for one product in its own repository.
Store the documentation for another product in the same repository as the product code.
Store diagrams and example code in a separate repository.

The site UI is also stored in its own repository so the web team can maintain the UI without conflicting with the docs team workflow or content files.
////

=== Remote and local, private and public, bare and non-bare

// on website
Antora can collect files from a combination of local, remote public, and remote private repositories over any protocol supported by git.

== Site Orchestration for Everyone

=== The Antora Playbook

// on website

With an Antora playbook, anyone can assemble and generate a documentation site.
You don't need any build automation or system administration knowledge to create or use an Antora playbook.
A playbook is a straight-forward set of concise instructions that tells Antora what documentation to assemble, where to find it, and what UI to apply to the site.

==== How?

Antora runs from a purely configuration playbook project that provides instructions about what to generate and where to get it.
// It's controllable by a non-developer and provides a non-technical interface.

=== Run your playbook using the Antora CLI

//on website
Just point the Antora CLI at your playbook to configure and execute the default site generator.
Out comes your site!

=== Site Remixes for Any Occasion

// on website
With Antora’s playbooks, you can control which versions of the documentation get published to a site.
You can even tell Antora to use uncommitted changes when generating the site on a local machine.

Here are a few common site remixes:

* Publish the documentation for all products and product versions to a single site.
* Publish the documentation for each product to its own unique site with its own branded UI.
* Only publish documentation for stable product versions to a docs site.
* Publish a product's beta release documentation to a private site.
* Launch a new product's documentation on a temporary URL with a custom UI, and, at the same time, publish it to a canonical docs site URL with your standard UI.
* And many more ...

// Can have multiple playbooks that can be as similar or different as you want them to be.
// Antora fetches the content it needs on demand.

=== Open Architecture

// on website
Antora features an open architecture.
Despite providing an opinionated default site generator, you’re free to reuse the core components to assemble your own custom generator pipeline.

=== Environment Portability
// Publish anywhere without modifying documentation files
// Multi-channel publishing <- website title

//on website
Antora automatically adapts a docs site to its publishing environment.
Whether the site is published locally, to revolving testing, QA, or staging environments, or to an alternate URL for a campaign or event, the site is fully functional.
This environment portability is possible because Antora’s cross referencing system isn’t based on a hardcoded base URL or system path.
//Because Antora decouples the content files, navigation, site UI, and other documentation assets from their source infrastructure, it can automatically adapt the docs site to its publishing environment.

// === Configuration as Code

== Minimal markup, maximum functionality

Mark up content with AsciiDoc's lightweight yet comprehensive syntax.

Features include:
// strikethrough?

* Bold, italic, monospace, highlight, and custom text roles
* Special Characters, font icons, and UI references (kbd:[Ctrl+T])
* Ordered, unordered, definition, and interactive task lists
* Videos (self-hosted, YouTube, Vimeo) and audio with display and user-control options
* Images and icons, inline or block, with display options
* Tables
* Notices (admonitions), quotes, and sidebars
* Metadata and taxonomy
* Cross references, in-page and page-to-page.
* Section, block and inline IDs
* Source code with copy and paste friendly callouts, callout icons, wrap or scroll controls, and syntax highlighting.
Source code can be written in the document or sourced from another file using an include directive.
* Include directive with tags.
Select all or parts of a file and insert it into another file.
* Table of Contents
* Math
* Page, section, asset, block and line controls to optimize reader experience (Rx)
//* Glossary, Bibliography, Appendix, and Index

=== No markup plugins, extensions, or shortcodes needed
// Fully integrated, lightweight and comprehensive markup

AsciiDoc's extensive feature set is available right out of the box with Antora.
There's no need to find, install, and manage plugins, extensions, or scripts to add basic capabilities to the syntax.

=== Tooling

Write and preview AsciiDoc with text editors and IDEs like Atom, Brackets, and IntelliJ.
GitLab and GitHub render AsciiDoc files right in the browser.
AsciiDoc files can also be previewed in Chrome and Firefox with the Asciidoctor extension.

=== Responsive maintainers

// on website
Antora’s core developers help lead the Asciidoctor organization, home of the AsciiDoc syntax and Asciidoctor, the AsciiDoc processor.
Due to this direct relationship, Antora is always in sync with AsciiDoc and Asciidoctor features.

=== No flavor lock-in
// Single source portability / usability
// Real single source portability

// on website
Documentation written with AsciiDoc works with all of the software and tools in the Asciidoctor ecosystem.
You don’t need to worry about incompatible syntax or lost functionality.

AsciiDoc files can also be used by other tools such as Asciidoctor PDF and EPUB, Asciidoctor HTML presentation extensions for Bespoke, Reveal.js.
This means your AsciiDoc files are immediately compatible with all of the other software that uses AsciiDoc (editors, HTML presentations, Asciidoctor PDF and EPUB generators, etc.).
You don't need to worry about errors, lost functionality, or downgraded processing performance.

=== Extension API

// on website
Custom syntax and output transformations can be added as discrete extensions with Asciidoctor’s extension API.

== Cross References

=== Impervious to file systems, environments, and URLs
// Decoupled from filesystems, environments, and URLs <- title on website

//on website
It doesn’t matter where you publish your documentation site &mdash; on a local machine, a staging environment, or a production environment &mdash; the page-to-page cross references always work.
That’s because Antora’s page referencing system isn’t coupled to filesystem paths or URLs.


////
Learn More link

The Learn More link would lead to a more detailed feature page (probably need diagrams) that would show a number of the scenarios the cross references aren't stymied by.
////


=== Writer-friendly cross references

Writers don't need to remember or construct the path to a file or a page's URL.
In most cases, to create a cross reference, all you need to know is the filename of the page you want to link to.

////
Learn More link

The Learn More link would lead to a docs page and/or a more detailed feature page that would show a number of the use cases the cross references can handle.
////

////
.Notes
--
- Page referencing system (page ID: component, version, module, local path) with contextual shorthand; works across repositories; agnostic to storage.
- Cross reference syntax that can link a file to a file in the same folder AND to a file that is in another repository?
--

=== Cross references are automatically validated and managed

No more broken links.
Antora automatically checks and validates cross references prior to deploying a site.
If errors are found the pipeline pauses and notifies the writer with a list of invalid cross references and the files they're located in.
////

== Site Navigation

=== Described using AsciiDoc lists

A site’s navigation is described by one or more AsciiDoc lists that are stored alongside your documentation files.
Links are added using the same cross referencing syntax used in the pages themselves.
There's no special syntax or UI template logic to learn.

=== No content or organization restrictions

// on website
Add normal text, images, icons, and external links to the navigation.
Order and nest the links in the arrangement of your choice.

////
.Notes
(l) describe and control the navigation structure as a content concern; navigation stored with the content; author-controlled, yet can still be manipulated by the UI / designer

(m) use same system to describe navigation references as is used to create references between pages; consistency of references; page reference system are used in navigation, xrefs, and aliases

. page references in navigation files match system used by pages. [m]

=== How?

. reads navigation tree from dedicated AsciiDoc files; converts to navigation model; model is prepared per page and passed to template to be converted to HTML. [l] [m]
////

== Site UI and Theme

=== Discrete UI

Site UIs are stored in separate repositories so that developing, testing, and publishing new or updated themes has no impact on your documentation files or their repositories.
Antora retrieves the specified site UI just like it fetches the content files from their repositories.

=== Web team workspaces

Web teams can use their preferred repository structure, development workflow and release schedule since the site UI isn't stored with the documentation.

=== Hot Swap UIs

Switch between site UIs just by changing the UI address in the playbook.

////
.Notes
****
* page_layout attribute in document controls which layout template is applied to page.

* populates templates with a model containing info about the site and backing content.

* UI project fits with designer's workflow and built using tools familiar to designer; standard setup

* (f) No coupling of content to output; failure to decouple UI hinders content reuse

* (g) Rebrand without modifying content

* (h) Use templates to make HTML match your playbook and/or environment; put power to control UI in your hands; shifts some of the decisions to the site manager (distributes roles and responsibilities); template w/ placeholders + UI model = HTML page
****
////

== URLs and Routes

=== URLs for humans and SEO

A page’s URL is derived from its filename, yet still isolated from its full filesystem path.

=== Route and redirect controls

Routes and redirects can be specified at the site, component, version, module and page level.

== Metadata and Taxonomy

=== Multi-level metadata controls

Add and remove metadata at the site, component, module, and page level using AsciiDoc attributes.

=== Built-in and custom taxonomy types

Assign taxonomy types and values at the site or page level.
