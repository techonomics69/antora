= Run Antora to Generate Your Site
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
// Settings
:idprefix:
:idseparator: -
:experimental:
:linkattrs:
// URIs
:uri-demo-playbook: https://gitlab.com/antora/demo/demo-site/blob/master/demo-site.yml
:uri-ssh-key: https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent

Assumptions:

* [x] You've xref:install/install-antora.adoc[installed the Antora CLI and a site generator pipeline].
* [x] You have your own xref:playbook:playbook.adoc[playbook], or you're using the Demo playbook.
* [x] Your playbook is configured to access at least one of your own  xref:component-structure.adoc[documentation component repositories] or Antora's Demo docs components.
* [x] Your playbook is configured to use a custom UI bundle or Antora's default UI bundle.

On this page, you'll learn:

* [x] How to run the default Antora pipeline.
* [x] Where files are cached.
* [x] How to update the cache.
* [x] How to preview a generated site locally.

////
To generate a site with Antora, you need a playbook file that points to at least one content source repository (aka docs component) and a UI bundle.
////

== Antora Demo

You don't need to set up a docs component or create a UI to try out Antora.
We've set up a playbook file, several content source repositories, and provided a default UI bundle.
As soon as you've installed Antora, you can run Antora with the Demo materials and explore its capabilities.
The instructions and examples on this page will show you how to operate Antora with the Demo materials.

== Choose a Playbook

To produce a documentation site, Antora needs a playbook.
First, you'll need to create or choose a directory where you'll store the playbook and where the generated site files will be saved (assuming you use the default output configuration).

For the examples on this page, we'll use the Demo materials.

. Open a terminal and make a new directory named [.path]_demo-site_.
+
[source]
$ mkdir demo-site

. Switch (`cd`) into the directory you just made.
+
[source]
$ cd demo-site

. Using your preferred text editor or IDE, create a new playbook file named [.path]_demo-site.yml_ and populate it with the contents of the following example.
You can also download {uri-demo-playbook}[the Demo site playbook] from the Demo repository.
+
.demo-site.yml
[source,yaml]
----
site:
  title: Antora Demo Site
  url: https://demo.antora.org
  start_page: component-b::index
content:
  sources:
  - url: https://gitlab.com/antora/demo/demo-component-a.git
    branches: master
  - url: https://gitlab.com/antora/demo/demo-component-b.git
    branches: [v2.0, v1.0]
    start_path: docs
ui:
  bundle:
    url: https://gitlab.com/antora/antora-ui-default/-/jobs/artifacts/master/raw/build/ui-bundle.zip?job=bundle-stable
    snapshot: true
----

. Save the playbook as [.path]_demo-site.yml_ in the [.path]_demo-site_ directory you made in Step 1.

== Run Antora

. To generate the site with the default site generator pipeline, point the `antora` command at your playbook file.
In the terminal, type:
+
[source]
demo-site $ antora demo-site.yml
+
First, Antora will clone the content repositories.
The cloning progress of each repository is displayed in the terminal.
+
.Repository cloning progress
[source]
----
[clone] https://gitlab.com/antora/demo/demo-component-a.git [################]
[clone] https://gitlab.com/antora/demo/demo-component-b.git [################]
----
+
Once cloning is complete, Antora converts the AsciiDoc pages to embeddable HTML, wraps the HTML in the UI page templates, then assembles the pages into a site under the destination folder, which defaults to _build/site_.

. Antora has completed generation when the command prompt (`$`) reappears in the terminal.

. Switch into the site folder (`cd`) and list (`ls`) its contents.
+
[source]
----
demo-site $ cd build/site/
site $ ls
_  component-a  component-b  index.html  sitemap-component-a.xml sitemap-component-b.xml  sitemap.xml
----
+
You should see the first page of your documentation site, in this case [.path]_index.html_.

. In some operating systems, you can open the site directly from the command line by typing `open`, followed by the name of the HTML file.
+
[source]
----
site $ open index.html
----
+
Or, you can navigate to an HTML page inside the destination folder in your browser.
If you've been following along with the Demo materials, look for the file `/demo-site/build/site/index.html`.

[#cache]
=== Cache

When Antora runs the first time, it will cache resources in a cache directory.
Antora caches two types of resources:

* cloned git repositories
* downloaded UI bundles

These resources are stored inside the cache directory, organized under the [.path]_content_ and [.path]_ui_ folders, respectively.
The xref:playbook:configure-runtime.adoc#default-cache[default cache directory] varies by operating system.
You can override the default cache location using the xref:playbook:configure-runtime.adoc[runtime.cache_dir key] in the playbook, the xref:cli.adoc#cli-options[--cache-dir] CLI option, or the `$ANTORA_CACHE_DIR` environment variable.

If you want to update the cache on subsequent runs, pass the xref:cli.adoc#cli-options[--pull switch] to the Antora CLI.
This switch will force Antora to run a fetch operation on each repository it previously cloned.
It will also force Antora to download a fresh copy of the UI bundle, if remote.

If you want to clear the cache altogether, you'll need to locate the Antora cache directory on your system and delete it.

[#using-private-repositories]
=== Private GitHub repositories

If your playbook uses private repositories, you must have an SSH agent running with the identity (i.e., SSH key) you've linked to your GitHub account.
Otherwise, Antora will fail.

The SSH agent allows Antora to authenticate with private repositories on your behalf.
To learn how to generate an SSH key, start the SSH agent, add your identity, and add your SSH key to your GitHub account, please refer to the article {uri-ssh-key}[Generating a new SSH key and adding it to the ssh-agent^].

== Local Site Preview

Since Antora generates static sites, *you do not need to publish the site to a server in order to preview it*.

To view the site, navigate to any HTML page inside the destination folder in your browser.
If your using the Demo, look for the file `/demo-site/build/site/index.html`.

=== Optional: Run local server

A site generated by Antora is designed to be viewable without a web server.
However, you may need to view your site through a web server to test certain features, such as indexified URLs or caching.
You can use the serve package for this purpose.

Install the serve package globally using npm:

[source]
$ npm i -g serve

That puts a command by the same name on your PATH.
Now launch the web server by pointing it at the location of the generated site.
In the terminal, type `serve build/site`.
After executing the command, a the local address should be displayed in your terminal.

[source]
----
demo-site $ serve build/site

   ┌─────────────────────────────────────────────────┐
   │                                                 │
   │   Serving!                                      │
   │                                                 │
   │   - Local:            http://localhost:5000     │
   │   - On Your Network:  http://192.168.1.9:5000   │
   │                                                 │
   │   Copied local address to clipboard!            │
   │                                                 │
   └─────────────────────────────────────────────────┘
----

Paste the provided URL into the location bar of your browser to view your site through a local web server.

////
When generation is complete, you'll see a URL in the terminal that is specific to your machine.

[.output]
....
Loading theme bundle from GitHub release: ...
Using content from repository: ...
...
Finished in 0:00:45
Site can be viewed at file:///home/user/projects/docs-site/build/site
....

You can follow this URL to preview the site locally in your browser.

Depending on what you built, you may have to navigate to an HTML file from that location to see the actual site.


The script loads and executes Antora and passes any trailing configuration flags and switches.
// Move this to a fragment or file in the playbook module
For example, you can specify a custom playbook as follows:

 $ node generate --playbook=custom-site

Depending on your internet connection speed, it may take anywhere from a few seconds to several minutes to complete the generation process the first time you run it.
That's because, first, Antora has to download (i.e., `git clone`) all the repositories specified in the playbook.

When you run Antora again, the repositories are automatically updated (i.e., `git pull`).
These subsequent runs complete much faster because only the changes to the repositories have to be downloaded.

The repositories are cached under the [.path]_build/sources/_ directory.
If you remove the [.path]_build/_ directory, the repositories will have to be downloaded again from scratch.
////
